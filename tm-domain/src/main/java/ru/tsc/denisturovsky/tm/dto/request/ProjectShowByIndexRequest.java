package ru.tsc.denisturovsky.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectShowByIndexRequest extends AbstractUserRequest {
    
    @Nullable
    private Integer index;

    public ProjectShowByIndexRequest(@Nullable String token) {
        super(token);
    }

}
