package ru.tsc.denisturovsky.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    IAuthEndpoint getAuthEndpointClient();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectEndpoint getProjectEndpointClient();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISystemEndpoint getSystemEndpointClient();

    @NotNull
    ITaskEndpoint getTaskEndpointClient();

    @NotNull
    ITokenService getTokenService();

    @NotNull
    IUserEndpoint getUserEndpointClient();

}
