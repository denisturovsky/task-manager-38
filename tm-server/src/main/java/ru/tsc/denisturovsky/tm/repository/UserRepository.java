package ru.tsc.denisturovsky.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.api.DBConstants;
import ru.tsc.denisturovsky.tm.api.repository.IUserRepository;
import ru.tsc.denisturovsky.tm.enumerated.Role;
import ru.tsc.denisturovsky.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public User add(@NotNull final User user) throws Exception {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                getTableName(), DBConstants.COLUMN_ID, DBConstants.COLUMN_LOGIN, DBConstants.COLUMN_PASSWORD,
                DBConstants.COLUMN_EMAIL, DBConstants.COLUMN_LOCKED, DBConstants.COLUMN_FIRST_NAME,
                DBConstants.COLUMN_LAST_NAME, DBConstants.COLUMN_MIDDLE_NAME, DBConstants.COLUMN_ROLE
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getId());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPasswordHash());
            statement.setString(4, user.getEmail());
            statement.setBoolean(5, user.getLocked());
            statement.setString(6, user.getFirstName());
            statement.setString(7, user.getLastName());
            statement.setString(8, user.getMiddleName());
            statement.setString(9, user.getRole().toString());
            statement.executeUpdate();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password
    ) throws Exception {
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setRole(Role.USUAL);
        return add(user);
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email
    ) throws Exception {
        @NotNull final User user = create(login, password);
        user.setEmail((email == null) ? "" : email);
        return user;
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final Role role
    ) throws Exception {
        @NotNull final User user = create(login, password);
        if (role != null) user.setRole(role);
        return user;
    }

    @NotNull
    @Override
    public User fetch(@NotNull final ResultSet row) throws Exception {
        @NotNull final User user = new User();
        user.setId(row.getString(DBConstants.COLUMN_ID));
        user.setLogin(row.getString(DBConstants.COLUMN_LOGIN));
        user.setPasswordHash(row.getString(DBConstants.COLUMN_PASSWORD));
        user.setEmail(row.getString(DBConstants.COLUMN_EMAIL));
        user.setLocked(row.getBoolean(DBConstants.COLUMN_LOCKED));
        user.setFirstName(row.getString(DBConstants.COLUMN_FIRST_NAME));
        user.setLastName(row.getString(DBConstants.COLUMN_LAST_NAME));
        user.setMiddleName(row.getString(DBConstants.COLUMN_MIDDLE_NAME));
        user.setRole(Role.valueOf(row.getString(DBConstants.COLUMN_ROLE)));
        return user;
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) throws Exception {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? LIMIT 1", getTableName(), DBConstants.COLUMN_EMAIL);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) throws Exception {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? LIMIT 1", getTableName(), DBConstants.COLUMN_LOGIN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @NotNull
    @Override
    protected String getTableName() {
        return DBConstants.TABLE_USER;
    }

    @NotNull
    @Override
    public Boolean isEmailExists(@NotNull final String email) throws Exception {
        return findByEmail(email) != null;
    }

    @NotNull
    @Override
    public Boolean isLoginExists(@NotNull final String login) throws Exception {
        return findByLogin(login) != null;
    }

    @Override
    public void update(@NotNull final User user) throws Exception {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(), DBConstants.COLUMN_LOGIN, DBConstants.COLUMN_PASSWORD, DBConstants.COLUMN_EMAIL,
                DBConstants.COLUMN_LOCKED, DBConstants.COLUMN_FIRST_NAME, DBConstants.COLUMN_LAST_NAME,
                DBConstants.COLUMN_MIDDLE_NAME, DBConstants.COLUMN_ROLE, DBConstants.COLUMN_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPasswordHash());
            statement.setString(3, user.getEmail());
            statement.setBoolean(4, user.getLocked());
            statement.setString(5, user.getFirstName());
            statement.setString(6, user.getLastName());
            statement.setString(7, user.getMiddleName());
            statement.setString(8, user.getRole().toString());
            statement.setString(9, user.getId());
            statement.executeUpdate();
        }
    }

}