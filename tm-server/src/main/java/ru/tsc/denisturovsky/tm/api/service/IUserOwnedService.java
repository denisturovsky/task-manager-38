package ru.tsc.denisturovsky.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.api.repository.IUserOwnedRepository;
import ru.tsc.denisturovsky.tm.enumerated.Sort;
import ru.tsc.denisturovsky.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M> {

    @NotNull
    List<M> findAll(
            @Nullable String userId,
            @Nullable Sort sort
    ) throws Exception;

    @Nullable
    M removeOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    M removeOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

}
