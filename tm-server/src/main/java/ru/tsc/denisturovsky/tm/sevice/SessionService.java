package ru.tsc.denisturovsky.tm.sevice;

import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.api.repository.ISessionRepository;
import ru.tsc.denisturovsky.tm.api.service.IConnectionService;
import ru.tsc.denisturovsky.tm.api.service.ISessionService;
import ru.tsc.denisturovsky.tm.model.Session;
import ru.tsc.denisturovsky.tm.repository.SessionRepository;

import java.sql.Connection;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    public ISessionRepository getRepository(@NotNull Connection connection) {
        return new SessionRepository(connection);
    }

}