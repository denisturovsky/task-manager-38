package ru.tsc.denisturovsky.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.denisturovsky.tm.api.repository.IUserRepository;
import ru.tsc.denisturovsky.tm.api.service.IConnectionService;
import ru.tsc.denisturovsky.tm.api.service.IPropertyService;
import ru.tsc.denisturovsky.tm.enumerated.Role;
import ru.tsc.denisturovsky.tm.marker.UnitCategory;
import ru.tsc.denisturovsky.tm.model.User;
import ru.tsc.denisturovsky.tm.sevice.ConnectionService;
import ru.tsc.denisturovsky.tm.sevice.PropertyService;
import ru.tsc.denisturovsky.tm.util.HashUtil;

import java.sql.Connection;

import static ru.tsc.denisturovsky.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private static final IUserRepository repository = new UserRepository(connection);

    @BeforeClass
    public static void setUp() throws Exception {
        repository.add(USER_TEST);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable User user = repository.findOneById(USER_TEST.getId());
        if (user != null) repository.remove(user);
        connection.commit();
        connection.close();
    }

    @Test
    public void add() throws Exception {
        Assert.assertNotNull(repository.add(ADMIN_TEST));
        @Nullable final User user = repository.findOneById(ADMIN_TEST.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_TEST.getId(), user.getId());
    }

    @After
    public void after() throws Exception {
        @Nullable User user = repository.findOneById(ADMIN_TEST.getId());
        if (user != null) repository.remove(user);
    }

    @Test
    public void create() throws Exception {
        @NotNull final User user = repository.create(ADMIN_TEST_LOGIN, HashUtil.salt(propertyService, ADMIN_TEST_PASSWORD));
        Assert.assertEquals(ADMIN_TEST.getLogin(), user.getLogin());
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
    }

    @Test
    public void createWithEmail() throws Exception {
        @NotNull final User user = repository.create(ADMIN_TEST_LOGIN, HashUtil.salt(propertyService, ADMIN_TEST_PASSWORD), ADMIN_TEST_EMAIL);
        Assert.assertEquals(ADMIN_TEST.getLogin(), user.getLogin());
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
        Assert.assertEquals(ADMIN_TEST.getEmail(), user.getEmail());
    }

    @Test
    public void createWithRole() throws Exception {
        @NotNull final User user = repository.create(ADMIN_TEST_LOGIN, HashUtil.salt(propertyService, ADMIN_TEST_PASSWORD), Role.ADMIN);
        Assert.assertEquals(ADMIN_TEST.getLogin(), user.getLogin());
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
        Assert.assertEquals(Role.ADMIN, user.getRole());
    }

    @Test
    public void existsById() throws Exception {
        Assert.assertFalse(repository.existsById(NON_EXISTING_USER_ID));
        Assert.assertTrue(repository.existsById(USER_TEST.getId()));
    }

    @Test
    public void findByEmail() throws Exception {
        @Nullable final User user = repository.findByEmail(USER_TEST.getEmail());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST.getId(), user.getId());
    }

    @Test
    public void findByLogin() throws Exception {
        @Nullable final User user = repository.findByLogin(USER_TEST.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST.getId(), user.getId());
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertNull(repository.findOneById(NON_EXISTING_USER_ID));
        @Nullable final User user = repository.findOneById(USER_TEST.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST.getId(), user.getId());
    }

    @Test
    public void isEmailExists() throws Exception {
        Assert.assertTrue(repository.isEmailExists(USER_TEST.getEmail()));
    }

    @Test
    public void isLoginExists() throws Exception {
        Assert.assertTrue(repository.isLoginExists(USER_TEST.getLogin()));
    }

    @Test
    public void remove() throws Exception {
        repository.add(ADMIN_TEST);
        @Nullable final User removedUser = repository.remove(ADMIN_TEST);
        Assert.assertNotNull(removedUser);
        Assert.assertEquals(ADMIN_TEST.getId(), removedUser.getId());
        Assert.assertNull(repository.findOneById(ADMIN_TEST.getId()));
    }

}